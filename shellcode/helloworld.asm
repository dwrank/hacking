; syscall(rdi, rsi, rdx, r10, r8, r9)
; syscalls are defined in /usr/include/asm/unistd_64.h

section .data
msg db "Hello, world!", 0x0a

section .text
global _start

_start:

; write(1, msg, 14)
mov rax, 1   ; write
mov rdi, 1   ; 1 = stdout
mov rsi, msg
mov rdx, 14  ; 14 bytes
syscall

; exit(0)
mov rax, 60  ; exit
mov rbx, 0
syscall
